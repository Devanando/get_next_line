/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strchr.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: dkroeke <dkroeke@student.codam.nl>           +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/03/22 12:41:19 by dkroeke        #+#    #+#                */
/*   Updated: 2019/04/12 17:36:00 by dkroeke       ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	int		i;
	int		j;
	char	*first;

	first = (char *)s;
	i = (int)ft_strlen(s);
	j = 0;
	while (j <= i)
	{
		if (*first == (char)c)
			return (first);
		first++;
		j++;
	}
	return (0);
}
